//
//  Bank_LocatorTests.m
//  Bank LocatorTests
//
//  Created by Vitali Kurlovich on 7/16/14.
//  Copyright (c) 2014 Vitali Kurlovich. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface Bank_LocatorTests : XCTestCase

@end

@implementation Bank_LocatorTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    double a = 1.0/2;
    double b = 3.0/2;
    
    XCTAssertEqual(([self getHalfA:1.0]+b)/2.0, 1.0, @"");
}


- (double)getHalfA:(double)a
{
    return a*a/2.0;
}

@end
