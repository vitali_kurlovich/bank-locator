//
//  BLPlaceInfo.h
//  Bank Locator
//
//  Created by Vitali Kurlovich on 7/16/14.
//  Copyright (c) 2014 Vitali Kurlovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BLWorkingInfo, BLPlaceDescription;

@interface BLPlaceInfo : NSObject

@property(nonatomic) NSNumber* lat;
@property(nonatomic) NSNumber* lon;

@property(nonatomic, copy) NSString* address;

@property(nonatomic) BLWorkingInfo* workingInfo;
@property(nonatomic) BLPlaceDescription *placeDescription;

@end
