//
//  BLPlaceInfoTableViewCellProtocol.h
//  Bank Locator
//
//  Created by Vitali Kurlovich on 7/16/14.
//  Copyright (c) 2014 Vitali Kurlovich. All rights reserved.
//



@protocol BLPlaceInfoTableViewCellProtocol <NSObject>
@property(nonatomic, readonly, weak) UILabel* addressLabel;
@property(nonatomic, readonly, weak) UILabel* sceduleLabel;
@property(nonatomic, readonly, weak) UILabel* infoLabel;
@end
