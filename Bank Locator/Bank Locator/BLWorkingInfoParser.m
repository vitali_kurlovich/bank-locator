//
//  BLWorkingInfoParser.m
//  Bank Locator
//
//  Created by Vitali Kurlovich on 7/16/14.
//  Copyright (c) 2014 Vitali Kurlovich. All rights reserved.
//

#import "BLWorkingInfoParser.h"

#import "BLWorkingInfo.h"

@implementation BLWorkingInfoParser
- (BLWorkingInfo*)parseDictionary:(NSDictionary*)dict {
    BLWorkingInfo* workingInfo = [[BLWorkingInfo alloc] init];
    workingInfo.note = dict[@"note"];
    return workingInfo;
}
@end
