//
//  BLPlaceInfo+MapKitAnnotation.m
//  Bank Locator
//
//  Created by Vitali on 7/16/14.
//  Copyright (c) 2014 Vitali Kurlovich. All rights reserved.
//

#import "BLPlaceInfo+MapKitAnnotation.h"

#import "BLPlaceDescription.h"
#import "BLWorkingInfo.h"

#import "NSNumber+CLLocationDegrees.h"

@implementation BLPlaceInfo (MapKitAnnotation)

#pragma mark - MKAnnotation

// Center latitude and longitude of the annotion view.
// The implementation of this property must be KVO compliant.
- (CLLocationCoordinate2D)coordinate {
    return CLLocationCoordinate2DMake([self.lat locationDegrees], [self.lon locationDegrees]);
}


// Title and subtitle for use by selection UI.
- (NSString*)title {
    return self.address;
}
- (NSString*)subtitle {
    return self.placeDescription.place;
}

// Called as a result of dragging an annotation view.
- (void)setCoordinate:(CLLocationCoordinate2D)newCoordinate {
    
}

@end
