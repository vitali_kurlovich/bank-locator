//
//  NSNumber+CLLocationDegrees.m
//  Bank Locator
//
//  Created by Vitali on 7/16/14.
//  Copyright (c) 2014 Vitali Kurlovich. All rights reserved.
//

#import "NSNumber+CLLocationDegrees.h"

static const double kScaleFactor = 0.000001;

@implementation NSNumber (CLLocationDegrees)

- (id)initWithLocationDegrees :(CLLocationDegrees)value {
    self = [self initWithDouble:(value/kScaleFactor)];
    return self;
}

+ (NSNumber *)numberWithLocationDegrees:(CLLocationDegrees)value {
    return [self numberWithDouble:(value/kScaleFactor)];
}

- (CLLocationDegrees)locationDegrees {
    return [self doubleValue]*0.000001;
}
@end
