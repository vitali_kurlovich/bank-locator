//
//  BLBankLocatorService.m
//  Bank Locator
//
//  Created by Vitali Kurlovich on 7/16/14.
//  Copyright (c) 2014 Vitali Kurlovich. All rights reserved.
//

#import "BLBankLocatorService.h"

#import <AFNetworking/AFNetworking.h>

#import "BLPlaceInfoCollectionParser.h"
#import "NSNumber+CLLocationDegrees.h"

static NSString *const kBaseUrl = @"http://locator.m-bank.by/geo-service/rest/bank-info/poi_nearest";

@interface BLBankLocatorService() {
    AFHTTPRequestOperationManager *_manager;
    BLPlaceInfoCollectionParser *_parser;
}

@property(nonatomic, weak) AFHTTPRequestOperation* currentRequestOperation;
@property(nonatomic, readonly) AFHTTPRequestOperationManager *manager;
@property(nonatomic, readonly) BLPlaceInfoCollectionParser *parser;

@property(nonatomic) BLPlaceInfoCollection* lastPlaceInfoCollection;

@end

@implementation BLBankLocatorService

- (AFHTTPRequestOperationManager*)manager {
    if (_manager == nil) {
        _manager = [AFHTTPRequestOperationManager manager];
        AFHTTPResponseSerializer *serializer = [[AFJSONResponseSerializer alloc ] init];
        _manager.responseSerializer = serializer;
    }
    return _manager;
}

- (BLPlaceInfoCollectionParser*)parser {
    if (_parser == nil) {
        _parser = [[BLPlaceInfoCollectionParser alloc] init];
    }
    return _parser;
}

- (void)placeInfoByLocation:(CLLocationDegrees)latitude longitude:(CLLocationDegrees)longitude{
   
    [self.currentRequestOperation cancel];
    self.lastPlaceInfoCollection = nil;
    
    NSNumber* lat = [NSNumber numberWithLocationDegrees:latitude];
    NSNumber* lon = [NSNumber numberWithLocationDegrees:longitude];
    NSString* center = [NSString stringWithFormat:@"%ld,%ld", (long)[lat integerValue], (long)[lon integerValue]];
    NSDictionary* params = @{@"p_version" : @"2", @"count": @"50", @"center" : center};
   
    __weak typeof(self) weakSelf = self;
    
    [self.delegate bankLocatorServiceDidBeginRequest:self];
    self.currentRequestOperation = [self.manager GET:kBaseUrl parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        BLPlaceInfoCollection* placeInfoCollection = [self.parser parseDictionary:responseObject];
        weakSelf.lastPlaceInfoCollection = placeInfoCollection;
        
        [weakSelf.delegate bankLocatorServiceDidFinishRequest:weakSelf withPlaceInfoCollection:placeInfoCollection error:nil];
       
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [weakSelf.delegate bankLocatorServiceDidFinishRequest:weakSelf withPlaceInfoCollection:nil error:error];
    }];
    
   
}

- (void)reset {
    self.lastPlaceInfoCollection = nil;
}

- (BOOL)isRequestStarted {
    return [self.currentRequestOperation isExecuting];
}

@end
