//
//  BLPlaceDescriptionParser.m
//  Bank Locator
//
//  Created by Vitali Kurlovich on 7/16/14.
//  Copyright (c) 2014 Vitali Kurlovich. All rights reserved.
//

#import "BLPlaceDescriptionParser.h"

#import "BLPlaceDescription.h"

@implementation BLPlaceDescriptionParser
- (BLPlaceDescription*)parseDictionary:(NSDictionary*)dict {
    BLPlaceDescription* placeDescription = [[BLPlaceDescription alloc] init];
    placeDescription.place = dict[@"place"];
    return placeDescription;
}

@end
