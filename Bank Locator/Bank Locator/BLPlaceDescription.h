//
//  BLPlaceDescription.h
//  Bank Locator
//
//  Created by Vitali Kurlovich on 7/16/14.
//  Copyright (c) 2014 Vitali Kurlovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BLPlaceDescription : NSObject
@property(nonatomic, copy) NSString* place;
@end
