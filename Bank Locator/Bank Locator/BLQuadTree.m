//
//  BLQuadTree.m
//  Bank Locator
//
//  Created by Vitali Kurlovich on 15.11.14.
//  Copyright (c) 2014 Vitali Kurlovich. All rights reserved.
//

#import "BLQuadTree.h"

#import "BLQuad.h"

@interface BLQuadTree()
@property (nonatomic, readonly) BLQuad* root;
@end

@implementation BLQuadTree

@end
