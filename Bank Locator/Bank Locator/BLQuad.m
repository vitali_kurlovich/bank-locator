//
//  BLQuad.m
//  Bank Locator
//
//  Created by Vitali Kurlovich on 15.11.14.
//  Copyright (c) 2014 Vitali Kurlovich. All rights reserved.
//

#import "BLQuad.h"


@interface BLQuad()

@property (nonatomic) BLQuad* root;
@end

@implementation BLQuad

- (void)setTopLeftQuad:(BLQuad *)topLeftQuad
{
    topLeftQuad.root = self;
    _topLeftQuad = topLeftQuad;
}

- (void)setTopRightQuad:(BLQuad *)topRightQuad
{
    topRightQuad.root = self;
    _topRightQuad = topRightQuad;
}

- (void)setBottomLeftQuad:(BLQuad *)bottomLeftQuad
{
    bottomLeftQuad.root = self;
    _bottomLeftQuad = bottomLeftQuad;
}

- (void)setBottomRightQuad:(BLQuad *)bottomRightQuad
{
    bottomRightQuad.root = self;
    _bottomRightQuad = bottomRightQuad;
}

@end
