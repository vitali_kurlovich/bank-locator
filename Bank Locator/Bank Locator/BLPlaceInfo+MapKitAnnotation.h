//
//  BLPlaceInfo+MapKitAnnotation.h
//  Bank Locator
//
//  Created by Vitali on 7/16/14.
//  Copyright (c) 2014 Vitali Kurlovich. All rights reserved.
//

#import "BLPlaceInfo.h"

#import <MapKit/MapKit.h>

@interface BLPlaceInfo (MapKitAnnotation) <MKAnnotation>

@end
