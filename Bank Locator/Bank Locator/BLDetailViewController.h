//
//  BLDetailViewController.h
//  Bank Locator
//
//  Created by Vitali Kurlovich on 7/16/14.
//  Copyright (c) 2014 Vitali Kurlovich. All rights reserved.
//

#import <MapKit/MapKit.h>

@class BLPlaceInfoCollection, BLPlaceInfo;

@interface BLDetailViewController : UIViewController <UISplitViewControllerDelegate>
@property(nonatomic) BLPlaceInfoCollection *placeInfoCollection;

@property(nonatomic) BLPlaceInfo* selectedPlaceInfo;

- (void)zoomToAnnotationsRect:(BOOL)animation;



@end
