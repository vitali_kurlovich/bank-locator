//
//  BLDetailViewController.m
//  Bank Locator
//
//  Created by Vitali Kurlovich on 7/16/14.
//  Copyright (c) 2014 Vitali Kurlovich. All rights reserved.
//

#import "BLDetailViewController.h"

#import <MapKit/MapKit.h>

#import "BLPlaceInfoCollection.h"


#import "NSNumber+CLLocationDegrees.h"
#import "BLPlaceInfo+MapKitAnnotation.h"

@interface BLDetailViewController ()
@property (strong, nonatomic) UIPopoverController *masterPopoverController;
@property(nonatomic, weak) IBOutlet MKMapView* mapView;
- (void)configureView;
@end

@implementation BLDetailViewController

#pragma mark - Managing the detail item



- (void)setPlaceInfoCollection:(BLPlaceInfoCollection *)placeInfoCollection {
    if (_placeInfoCollection != placeInfoCollection) {
        _placeInfoCollection = placeInfoCollection;
        
        [self configureView];
    }
    
    [self.masterPopoverController dismissPopoverAnimated:YES];
    
}

- (void)configureView
{
    // Update the user interface for the detail item.
    
    
    [self.mapView removeAnnotations:self.mapView.annotations];
    
    NSArray* items = self.placeInfoCollection.items;
    if (items) {
        [self.mapView addAnnotations:items];
    }
}

- (void)zoomToAnnotationsRect:(BOOL)animation {
    MKMapRect zoomRect = MKMapRectNull;
    for (id <MKAnnotation> annotation in self.mapView.annotations)
    {
        MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
        MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.1, 0.1);
        zoomRect = MKMapRectUnion(zoomRect, pointRect);
    }
    [self.mapView setVisibleMapRect:zoomRect animated:animation];
}

- (void)setCenterCoordinate:(CLLocationCoordinate2D)coordinate animated:(BOOL)animated {
    [self.mapView setCenterCoordinate:coordinate animated:animated];
}

- (void)selectAnnotation:(id <MKAnnotation>)annotation animated:(BOOL)animated {
    [self.mapView selectAnnotation:annotation animated:animated];
}

- (void)setSelectedPlaceInfo:(BLPlaceInfo *)selectedPlaceInfo {
    if (_selectedPlaceInfo != selectedPlaceInfo) {
        _selectedPlaceInfo = selectedPlaceInfo;
        
        [self setCenterCoordinate:CLLocationCoordinate2DMake([selectedPlaceInfo.lat locationDegrees], [selectedPlaceInfo.lon locationDegrees]) animated:YES];
        [self selectAnnotation:selectedPlaceInfo animated:YES];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self configureView];

}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self zoomToAnnotationsRect:YES];
    
    //[self setCenterCoordinate:CLLocationCoordinate2DMake([self.selectedPlaceInfo.lat locationDegrees], [self.selectedPlaceInfo.lon locationDegrees]) animated:YES];
    [self selectAnnotation:self.selectedPlaceInfo animated:YES];
    
    //self.mapView an
    // Dispose of any resources that can be recreated.
}

#pragma mark - Split view

- (void)splitViewController:(UISplitViewController *)splitController willHideViewController:(UIViewController *)viewController withBarButtonItem:(UIBarButtonItem *)barButtonItem forPopoverController:(UIPopoverController *)popoverController
{
    barButtonItem.title = NSLocalizedString(@"Master", @"Master");
    [self.navigationItem setLeftBarButtonItem:barButtonItem animated:YES];
    self.masterPopoverController = popoverController;
}

- (void)splitViewController:(UISplitViewController *)splitController willShowViewController:(UIViewController *)viewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    // Called when the view is shown again in the split view, invalidating the button and popover controller.
    [self.navigationItem setLeftBarButtonItem:nil animated:YES];
    self.masterPopoverController = nil;
}

@end
