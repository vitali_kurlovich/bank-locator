//
//  BLMasterViewController.h
//  Bank Locator
//
//  Created by Vitali Kurlovich on 7/16/14.
//  Copyright (c) 2014 Vitali Kurlovich. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BLDetailViewController;

@interface BLMasterViewController : UITableViewController

@property (strong, nonatomic) BLDetailViewController *detailViewController;

@end
