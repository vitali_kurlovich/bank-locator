//
//  BLPlaceInfoTableViewCell.m
//  Bank Locator
//
//  Created by Vitali Kurlovich on 7/16/14.
//  Copyright (c) 2014 Vitali Kurlovich. All rights reserved.
//

#import "BLPlaceInfoTableViewCell.h"

@interface BLPlaceInfoTableViewCell()
@property(nonatomic,  weak) IBOutlet UILabel* addressLabel;
@property(nonatomic,  weak) IBOutlet UILabel* sceduleLabel;
@property(nonatomic,  weak) IBOutlet UILabel* infoLabel;
@end

@implementation BLPlaceInfoTableViewCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
