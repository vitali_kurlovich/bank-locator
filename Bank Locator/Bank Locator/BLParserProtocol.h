//
//  BLParserProtocol.h
//  Bank Locator
//
//  Created by Vitali on 7/17/14.
//  Copyright (c) 2014 Vitali Kurlovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol BLParserProtocol <NSObject>
- (id)parseDictionary:(NSDictionary*)dict;
@end
