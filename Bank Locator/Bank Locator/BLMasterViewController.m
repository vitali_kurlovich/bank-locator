//
//  BLMasterViewController.m
//  Bank Locator
//
//  Created by Vitali Kurlovich on 7/16/14.
//  Copyright (c) 2014 Vitali Kurlovich. All rights reserved.
//

#import "BLMasterViewController.h"

#import <CoreLocation/CoreLocation.h>

#import "BLDetailViewController.h"
#import "BLBankLocatorService.h"

#import "BLPlaceInfoTableViewCellProtocol.h"

#import "BLPlaceInfoCollection.h"
#import "BLPlaceInfo.h"
#import "BLPlaceDescription.h"
#import "BLWorkingInfo.h"

#import "NSNumber+CLLocationDegrees.h"


@interface BLMasterViewController () <BLBankLocatorServiceDelegate, CLLocationManagerDelegate>{
    NSMutableArray *_objects;
    BLBankLocatorService* _bankLocatorService;
    CLLocationManager* _locationManager;
}

@property(nonatomic, readonly) BLBankLocatorService* bankLocatorService;
@property(nonatomic, readonly) CLLocationManager* locationManager;

@property(nonatomic) NSError* lastError;

@property(nonatomic, weak) IBOutlet UILabel* errorLabel;

@end

@implementation BLMasterViewController

#pragma mark - NSObject(UINibLoadingAdditions)

- (void)awakeFromNib
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        self.clearsSelectionOnViewWillAppear = NO;
        self.preferredContentSize = CGSizeMake(320.0, 600.0);
    }
    [super awakeFromNib];
}


#pragma mark - UIViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.detailViewController = (BLDetailViewController *)[[self.splitViewController.viewControllers lastObject] topViewController];
    
    UIRefreshControl *refresh = [[UIRefreshControl alloc] init];
    refresh.attributedTitle = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Refresh with Location", nil)];
    [refresh addTarget:self action:@selector(doRefresh)
      forControlEvents:UIControlEventValueChanged];
    
    self.refreshControl = refresh;
    
    [self refresh];
    [self doRefresh];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    //[self doRefresh];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showMap"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        
        BLPlaceInfoCollection* placeInfoCollection = self.bankLocatorService.lastPlaceInfoCollection;
        BLPlaceInfo* placeInfo = placeInfoCollection.items[indexPath.row];
        
        [[segue destinationViewController] setPlaceInfoCollection:self.bankLocatorService.lastPlaceInfoCollection];
        
        // [[segue destinationViewController] setCenterCoordinate:CLLocationCoordinate2DMake([placeInfo.lat locationDegrees], [placeInfo.lon locationDegrees]) animated:YES];
        
        [[segue destinationViewController] setSelectedPlaceInfo:placeInfo];
        
    }
}


#pragma mark - BLMasterViewController

- (BLBankLocatorService*)bankLocatorService {
    if (_bankLocatorService == nil) {
        _bankLocatorService = [[BLBankLocatorService alloc] init];
        _bankLocatorService.delegate = self;
    }
    return _bankLocatorService;
}

- (CLLocationManager*)locationManager {
    if (_locationManager == nil) {
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;
        _locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    }
    return _locationManager;
}

- (void)doRefresh {
    [self.bankLocatorService reset];
    
    //self.refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Fetch User Location", nil)];
    
    [self.locationManager startUpdatingLocation];
    
}

- (void)refresh {

    self.errorLabel.text = [self.lastError localizedDescription];
    [self.tableView reloadData];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    [self.locationManager stopUpdatingLocation];
     self.lastError = nil;
    
    //self.refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Fetch place info", nil)];
    [self.bankLocatorService placeInfoByLocation:newLocation.coordinate.latitude longitude:newLocation.coordinate.longitude];
    
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    self.lastError = error;
    if ([error code] != kCLErrorLocationUnknown) {
        
    }
    
    [self.refreshControl endRefreshing];
    //self.refreshControl.attributedTitle = nil;
    
    [self refresh];
}


#pragma mark - BLBankLocatorServiceDelegate

- (void)bankLocatorServiceDidFinishRequest:(BLBankLocatorService*)service withPlaceInfoCollection:(BLPlaceInfoCollection*)collection error:(NSError*)error {
    [self.refreshControl endRefreshing];
    //self.refreshControl.attributedTitle = nil;
    
    self.lastError = error;
    [self refresh];
    
    self.detailViewController.placeInfoCollection = collection;
    [self.detailViewController zoomToAnnotationsRect:YES];
    
}

- (void)bankLocatorServiceDidCancelRequest:(BLBankLocatorService*)service {
    [self.refreshControl endRefreshing];
    //self.refreshControl.attributedTitle = nil;
}

- (void)bankLocatorServiceDidBeginRequest:(BLBankLocatorService*)service {
    [self.refreshControl beginRefreshing];
    //self.refreshControl.attributedTitle = nil;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    BLPlaceInfoCollection* data = self.bankLocatorService.lastPlaceInfoCollection;
    
   
    
    return [data.items count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell<BLPlaceInfoTableViewCellProtocol> *cell = [tableView dequeueReusableCellWithIdentifier:@"BLPlaceInfoTableViewCell" forIndexPath:indexPath];
    BLPlaceInfoCollection* data = self.bankLocatorService.lastPlaceInfoCollection;
    BLPlaceInfo* place = data.items[indexPath.row];
    
    cell.addressLabel.text = place.address;
    cell.infoLabel.text = place.placeDescription.place;
    
    cell.sceduleLabel.text = place.workingInfo.note;
    return cell;
}


#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    BLPlaceInfoCollection* placeInfoCollection = self.bankLocatorService.lastPlaceInfoCollection;
    BLPlaceInfo* placeInfo = placeInfoCollection.items[indexPath.row];

    self.detailViewController.selectedPlaceInfo = placeInfo;
    
    
}



@end
