//
//  BLPlaceInfoCollectionParser.m
//  Bank Locator
//
//  Created by Vitali Kurlovich on 7/16/14.
//  Copyright (c) 2014 Vitali Kurlovich. All rights reserved.
//

#import "BLPlaceInfoCollectionParser.h"

#import "BLPlaceInfoCollection.h"
#import "BLPlaceInfoParser.h"

@interface BLPlaceInfoCollectionParser() {
    BLPlaceInfoParser* _placeInfoParser;
}
@property(nonatomic, readonly) BLPlaceInfoParser* placeInfoParser;
@end


@implementation BLPlaceInfoCollectionParser
- (BLPlaceInfoParser*)placeInfoParser {
    if (_placeInfoParser == nil) {
        _placeInfoParser = [[BLPlaceInfoParser alloc] init];
    }
    return _placeInfoParser;
}

- (BLPlaceInfoCollection*)parseDictionary:(NSDictionary*)dict {
    
    NSArray* poi = dict[@"poi"];
    NSMutableArray* result = [NSMutableArray arrayWithCapacity:[poi count]];
    
    for (NSDictionary* place in poi) {
        [result addObject:[self.placeInfoParser parseDictionary:place]];
    }
    
    BLPlaceInfoCollection* collection = [[BLPlaceInfoCollection alloc] init];
    collection.items = result;
    return collection;
}

@end
