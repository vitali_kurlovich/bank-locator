//
//  BLBankLocatorService.h
//  Bank Locator
//
//  Created by Vitali Kurlovich on 7/16/14.
//  Copyright (c) 2014 Vitali Kurlovich. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>

@protocol BLBankLocatorServiceDelegate;
@class BLPlaceInfoCollection;

@interface BLBankLocatorService : NSObject
@property(nonatomic, weak) id<BLBankLocatorServiceDelegate> delegate;

@property(nonatomic, readonly, getter = isRequestStarted) BOOL requestStarted;
@property(nonatomic, readonly) BLPlaceInfoCollection* lastPlaceInfoCollection;


- (void)placeInfoByLocation:(CLLocationDegrees)latitude longitude:(CLLocationDegrees)longitude;
- (void)reset;
@end

@protocol BLBankLocatorServiceDelegate <NSObject>
#pragma mark - BLBankLocatorServiceDelegate

- (void)bankLocatorServiceDidFinishRequest:(BLBankLocatorService*)service withPlaceInfoCollection:(BLPlaceInfoCollection*)collection error:(NSError*)error;
@optional
- (void)bankLocatorServiceDidCancelRequest:(BLBankLocatorService*)service;
- (void)bankLocatorServiceDidBeginRequest:(BLBankLocatorService*)service;
@end