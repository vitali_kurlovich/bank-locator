//
//  BLPlaceInfoParser.m
//  Bank Locator
//
//  Created by Vitali Kurlovich on 7/16/14.
//  Copyright (c) 2014 Vitali Kurlovich. All rights reserved.
//

#import "BLPlaceInfoParser.h"

#import "BLPlaceInfo.h"

#import "BLPlaceDescriptionParser.h"
#import "BLWorkingInfoParser.h"

@interface BLPlaceInfoParser() {
    BLPlaceDescriptionParser* _placeDescriptionParser;
    BLWorkingInfoParser* _workingInfoParser;
}
@property (nonatomic, readonly) BLPlaceDescriptionParser* placeDescriptionParser;
@property (nonatomic, readonly) BLWorkingInfoParser* workingInfoParser;

@end

@implementation BLPlaceInfoParser

- (BLPlaceInfo*)parseDictionary:(NSDictionary*)dict {
        
    BLPlaceInfo* placeInfo = [[BLPlaceInfo alloc] init];
    
    placeInfo.lat = dict[@"lat"];
    placeInfo.lon = dict[@"lon"];
    placeInfo.address = dict[@"addr"];
    placeInfo.placeDescription = [self.placeDescriptionParser parseDictionary:dict[@"text"]];
    placeInfo.workingInfo = [self.workingInfoParser parseDictionary:dict[@"wh"]];
    return placeInfo;
}

- (BLPlaceDescriptionParser*)placeDescriptionParser {
    if (_placeDescriptionParser == nil) {
        _placeDescriptionParser = [[BLPlaceDescriptionParser alloc] init];
    }
    return _placeDescriptionParser;
}

- (BLWorkingInfoParser*)workingInfoParser {
    if (_workingInfoParser == nil) {
        _workingInfoParser = [[BLWorkingInfoParser alloc] init];
    }
    return _workingInfoParser;
}
@end
