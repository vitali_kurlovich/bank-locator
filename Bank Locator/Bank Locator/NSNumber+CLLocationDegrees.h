//
//  NSNumber+CLLocationDegrees.h
//  Bank Locator
//
//  Created by Vitali on 7/16/14.
//  Copyright (c) 2014 Vitali Kurlovich. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>

@interface NSNumber (CLLocationDegrees)
- (id)initWithLocationDegrees :(CLLocationDegrees)value;

+ (NSNumber *)numberWithLocationDegrees:(CLLocationDegrees)value;

- (CLLocationDegrees)locationDegrees;

@end
