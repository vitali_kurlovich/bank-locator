//
//  BLQuad.h
//  Bank Locator
//
//  Created by Vitali Kurlovich on 15.11.14.
//  Copyright (c) 2014 Vitali Kurlovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BLQuad : NSObject

@property (nonatomic, readonly) BLQuad* root;

@property (nonatomic) BLQuad* topLeftQuad;
@property (nonatomic) BLQuad* topRightQuad;

@property (nonatomic) BLQuad* bottomLeftQuad;
@property (nonatomic) BLQuad* bottomRightQuad;

@end
