//
//  BLPlaceInfoCollectionParser.h
//  Bank Locator
//
//  Created by Vitali Kurlovich on 7/16/14.
//  Copyright (c) 2014 Vitali Kurlovich. All rights reserved.
//

#import "BLParserProtocol.h"

@class BLPlaceInfoCollection;

@interface BLPlaceInfoCollectionParser : NSObject<BLParserProtocol>
- (BLPlaceInfoCollection*)parseDictionary:(NSDictionary*)dict;
@end
